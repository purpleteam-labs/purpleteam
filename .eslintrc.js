module.exports = {
  extends: 'airbnb-base',
  rules: {
    'comma-dangle': ['error', 'never'],

    // specify the maximum length of a line in your program
    // http://eslint.org/docs/rules/max-len
    'max-len': ['error', 200, 2, {
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true
    }],
    // enforce consistent line breaks inside function parentheses
    // https://eslint.org/docs/rules/function-paren-newline
    'function-paren-newline': ['error', 'multiline'],
    'import/no-unresolved': ['error', { commonjs: true }],
    'no-unused-expressions': ['error', { allowShortCircuit: true, allowTernary: true }],
    'object-curly-newline': ['error', { multiline: true }],
    'no-multiple-empty-lines': ['error', { max: 2, maxBOF: 0, maxEOF: 1 }]
  },
  env: { node: true },
  parserOptions: { ecmaVersion: 2021 },
  // This is to support requires from the app root dir with the likes of app-module-path.
  // https://github.com/airbnb/javascript/issues/859
  // https://github.com/benmosher/eslint-plugin-import/tree/master/resolvers/node
  settings: {
    'import/resolver': {
      node: {
        paths: [
          `${process.cwd()}`
        ]
      }
    }
  }
};
